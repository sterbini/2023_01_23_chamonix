# %%
import os
import getpass
import logging
import sys
import json
import nx2pd as nx 
import pandas as pd

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

os.environ['PYSPARK_PYTHON'] = "./environment/bin/python"
username = getpass.getuser()
logging.info(f'Assuming that your kerberos keytab is in the home folder, ' 
      f'its name is "{getpass.getuser()}.keytab" '
      f'and that your kerberos login is "{username}".')

logging.info('Executing the kinit')
os.system(f'kinit -f -r 5d -kt {os.path.expanduser("~")}/{getpass.getuser()}.keytab {getpass.getuser()}');

# %%


from nxcals.spark_session_builder import get_or_create, Flavor

logging.info('Creating the spark instance')
spark = get_or_create(flavor=Flavor.LOCAL,                                         
conf={'spark.driver.maxResultSize': '8g',                                          
    'spark.executor.memory':'8g',                                                  
    'spark.driver.memory': '16g',                                                  
    'spark.executor.instances': '20',                                              
    'spark.executor.cores': '2',                                                   
    })                                                                             
sk  = nx.SparkIt(spark)
logging.info('Spark instance created.')
# %%

for my_snapshot in ['BBLR I_Earth Monitoring']:#['BBLR F8146', 'BBLR F8399']:
    var_names, my_dict = sk.get_snapshot(my_snapshot)

    t0 = pd.Timestamp(my_dict['getStartTime'], tz='UTC')
    t1 = pd.Timestamp(my_dict['getEndTime']  , tz='UTC')

    df = sk.get( t0,
                 t1,
                 var_names,
                 pandas_index_localize = False
                )
    df.to_parquet(my_snapshot+'.parquet')

#nx.pandas_index_localize(df)

# %%
var_names, my_dict = sk.get_snapshot('BBLR F8146')
t0 = pd.Timestamp(my_dict['getStartTime'], tz='UTC')
t1 = pd.Timestamp(my_dict['getEndTime']  , tz='UTC')
df = sk.get( t0,
             t1,
                 ['RBBCW.L1B1:ST_FAILURE_WIC'],
                 pandas_index_localize = False
                )

# %%
df = sk.nxcals_df(['RBBCW.L1B1:ST_FAILURE_WIC'], 
                        t0,
                        t1,
                        system_name="WINCCOA",
                        pandas_processing=[
                            nx.pandas_get, 
                            nx.pandas_pivot])
# %%
snapshot_name = 'BBLR F8146'
group = sk.group_service.findOne(sk.Groups.suchThat().name().eq(snapshot_name)).get()
var_names = [var.getVariableName() for var in sk.group_service.getVariables(group.getId())['getSelectedVariables']]
my_dict = dict(group.getProperties())
# %%
