To install a miniconda do

```bash
git clone ssh://git@gitlab.cern.ch:7999/lhclumi/lumi-followup.git
source ./lumi-followup/examples/nx2pd/make_it.sh

# Note that we install the package in editing mode
python -m pip install -e ./lumi-followup/nx2pd

#test
python ./lumi-followup/examples/nx2pd/001_simple.py
```
